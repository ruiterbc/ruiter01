Beginners Guide to Automated Workshops 
======================================
Basic template project to automate your online workshops. Includes basic reveal.js workshop outline, standard background template, and example slides to start you on your workshop development. Each commit generates a new online workshop site.
